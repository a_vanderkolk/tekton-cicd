Installed {{ .Chart.Name }} {{ .Chart.Version }} successfully with name {{ .Release.Name }} in {{ .Release.Namespace }}.

To uninstall run:
    helm uninstall {{ .Release.Name }} -n {{ .Release.Namespace }}