#!/bin/bash
# Script to download the yaml files from github
DOWNLOAD_FOLDER="../../templates/tasks/downloaded"

wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/buildah/0.2/buildah.yaml -O "${DOWNLOAD_FOLDER}/buildah.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/maven/0.2/maven.yaml -O "${DOWNLOAD_FOLDER}/maven.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/sonarqube-scanner/0.1/sonarqube-scanner.yaml -O "${DOWNLOAD_FOLDER}/sonarqube-scanner.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/curl/0.1/curl.yaml -O "${DOWNLOAD_FOLDER}/curl.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/helm-upgrade-from-source/0.3/helm-upgrade-from-source.yaml -O "${DOWNLOAD_FOLDER}/helm-upgrade-from-source.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/helm-upgrade-from-repo/0.2/helm-upgrade-from-repo.yaml -O "${DOWNLOAD_FOLDER}/helm-upgrade-from-repo.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/kubernetes-actions/0.2/kubernetes-actions.yaml -O "${DOWNLOAD_FOLDER}/kubernetes-actions.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/git-cli/0.2/git-cli.yaml -O "${DOWNLOAD_FOLDER}/git-cli.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/git-clone/0.4/git-clone.yaml -O "${DOWNLOAD_FOLDER}/git-clone.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/skopeo-copy/0.1/skopeo-copy.yaml -O "${DOWNLOAD_FOLDER}/skopeo-copy.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/yq/0.1/yq.yaml -O "${DOWNLOAD_FOLDER}/yq.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/npm/0.1/npm.yaml -O "${DOWNLOAD_FOLDER}/npm.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/argocd-task-sync-and-wait/0.1/argocd-task-sync-and-wait.yaml -O "${DOWNLOAD_FOLDER}/argocd-task-sync-and-wait.yaml"
wget https://raw.githubusercontent.com/tektoncd/catalog/main/task/tkn/0.2/tkn.yaml -O "${DOWNLOAD_FOLDER}/tkn.yaml"