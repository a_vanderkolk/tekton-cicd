#!/usr/bin/env python3
"""
Script to check of a Webhook already exists
"""

import requests
import json
import os


def get_headers(token):
    return {
        'Authorization': "token {}".format(token),
        'accept': 'application/json',
        'Content-Type': 'application/json'}


def get_webhook_url(webhook_protocol, webhook_host):
    return "{}://{}/".format(webhook_protocol, webhook_host)


def get_gitea_url(gitea_protocol, gitea_host):
    return "{}://{}".format(gitea_protocol, gitea_host)


def get_gitea_request_url(gitea_url, gitea_org, gitea_repo_name):
    return "{}/api/v1/repos/{}/{}/hooks".format(gitea_url, gitea_org, gitea_repo_name)


def call_get_webhooks(request_url, headers):
    resp = requests.get(url=request_url,
                        headers=headers)
    return resp


def has_webhook_url_configured(response_json, webhook_url):
    if len(response_json) < 1:
        return False

    for webhook in response_json:
        config = webhook['config']
        webhook_url_resp = config['url']
        if webhook_url_resp == webhook_url:
            return True

    return False


def write_result(result, result_path):
    with open(result_path, 'w') as file:
        file.write(str(result).upper())

def get_organization(repository_url):
    return repository_url.split('/')[3]


def get_repository(repository_url):
    return repository_url.split('/')[4].replace(".git", "")



def main():
    # TODO: Token komt waarschijnlijk uit een Environment variabele
    gitea_token = os.environ['AUTH_TOKEN']
    gitea_url = "$(params.GITEA_ADDRESS)"
    gitea_repository_url = "$(params.GITEA_REPOSITORY_URL)"
    webhook_address = "$(params.WEBHOOK_ADDRESS)"
    results_path = "$(results.WEBHOOK_EXISTS.path)"

    # # TODO: Delete me
    # gitea_url = r"http://192.168.178.21:12080"
    # gitea_repository_url = "ssh://git@192.168.178.21:12022/gitopsdemo/demo-web-app.git"
    # webhook_address = "https://webhook-java-source.labs.kolkos.nl"
    # results_path = "test"

    gitea_org = get_organization(gitea_repository_url)
    gitea_repo_name = get_repository(gitea_repository_url)  

    request_url = get_gitea_request_url(gitea_url, gitea_org, gitea_repo_name)
    headers = get_headers(gitea_token)
    resp = call_get_webhooks(request_url, headers)

    if resp.status_code != 200 and resp.status_code != 201:
        print("Error configuring the webhook (status code: {})".format(
            resp.status_code))
        print(resp.content)
        raise Exception("Cannot get configured webhooks")
    else:
        print("Successfully got webhooks response")
        data = resp.json()
        webhook_exists = has_webhook_url_configured(data, webhook_address)
        write_result(webhook_exists, results_path)


main()
