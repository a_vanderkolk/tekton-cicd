#!/usr/bin/env python3
"""
Script to create a webhook
"""

import requests
import os


def get_headers(token):
    return {
        'Authorization': "token {}".format(token),
        'accept': 'application/json',
        'Content-Type': 'application/json'}


def get_gitea_request_url(gitea_url, gitea_org, gitea_repo_name):
    return "{}/api/v1/repos/{}/{}/hooks".format(gitea_url, gitea_org, gitea_repo_name)


def get_data(webhook_url):
    return '{"type": "gitea", "config": { "url": "' + webhook_url + '", "content_type": "json"}, "events": ["pull_request", "pull_request_sync"], "active": true}'


def webhook_exists(results_path):
    if not os.path.isfile(results_path):
        print("Warning: File {} does not exists".format(results_path))
        return False

    with open(results_path, 'r') as file:
        result = file.read()
        print(result)

        if result == "TRUE":
            return True

        return False


def get_organization(repository_url):
    return repository_url.split('/')[3]


def get_repository(repository_url):
    return repository_url.split('/')[4].replace(".git", "")


def main():
    # check if the webhook already exists
    gitea_token = os.environ['AUTH_TOKEN']
    gitea_url = "$(params.GITEA_ADDRESS)"
    gitea_repository_url = "$(params.GITEA_REPOSITORY_URL)"
    webhook_address = "$(params.WEBHOOK_ADDRESS)"
    results_path = "$(results.WEBHOOK_EXISTS.path)"

    # # TODO: Delete me
    # gitea_url = r"http://192.168.178.21:12080"
    # gitea_repository_url = "ssh://git@192.168.178.21:12022/gitopsdemo/demo-web-app.git"
    # webhook_address = "https://webhook-java-source.labs.kolkos.nl"
    # results_path = "test"

    gitea_org = get_organization(gitea_repository_url)
    gitea_repo_name = get_repository(gitea_repository_url)

    if webhook_exists(results_path):
        print("Webhook already exists... Skipping")
        return

    request_url = get_gitea_request_url(gitea_url, gitea_org, gitea_repo_name)
    print(request_url)

    # configure webhook on tekton-tutorial-greeter
    data_webhook = get_data(webhook_address)
    headers = get_headers(gitea_token)

    resp = requests.post(url=request_url,
                         headers=headers,
                         data=data_webhook)

    if resp.status_code != 200 and resp.status_code != 201:
        print("Error configuring the webhook (status code: {})".format(
            resp.status_code))
        print(resp.content)
        raise Exception("Unable to create webhook")
    else:
        print("Configured webhook: " + webhook_address)


main()
