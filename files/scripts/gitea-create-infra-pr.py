#!/usr/bin/env python3
import requests
import os
import json

"""
Script to create a Pull Request for IaC Repositories
"""

development_branch = "$(params.development-branch)"
testing_branch = "$(params.testing-branch)"
staging_branch = "$(params.staging-branch)"
production_branch = "$(params.production-branch)"

def get_target_branch(merged_branch):
    switcher = {
        development_branch: testing_branch,
        testing_branch: staging_branch,
        staging_branch: production_branch
    }
    print("{} in {}".format(merged_branch, switcher))

    if merged_branch in switcher:
        return switcher[merged_branch]

    return development_branch
    

def get_organization(repository_url):
    return repository_url.split('/')[3]


def get_repository(repository_url):
    return repository_url.split('/')[4].replace(".git","")


def create_pull_request_url(gitea_url, organization, repository):
    return "{}/api/v1/repos/{}/{}/pulls".format(gitea_url, organization, repository)



def get_body(title, body, source, target):
    return {
        'base': target,
        'head': source,
        'body': body,
        'title': title
    }

def get_headers(token):
    return {
            'Authorization': "token {}".format(token),
            'accept': 'application/json',
            'Content-Type': 'application/json'}


def main():
    repository_url = "$(params.repository-url)"
    merged_branch = "$(params.merged-branch)"
    gitea_url = "$(params.gitea-url)"
    print("{} has been merged".format(merged_branch))

    source = merged_branch
    target = get_target_branch(source)
    title = "{} -> {}".format(source, target)
    
    token = os.environ['AUTH_TOKEN']

    if source == production_branch:
        print("Merged into production, no PR needed")
        return
    
    organization = get_organization(repository_url)
    repository = get_repository(repository_url)
    pull_request_url = create_pull_request_url(gitea_url, organization, repository)

    pr_message = "merging from {} to {}".format(source, target)
    request_body = get_body(title, pr_message, source, target)
    headers = get_headers(token)

    print(pull_request_url)
    print(pr_message)

    response = requests.post(pull_request_url, headers=headers, json=request_body)
    print(response.status_code)
    print(json.dumps(response.json(), indent=4))


main()