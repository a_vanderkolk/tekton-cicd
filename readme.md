# Tekton CICD Configuration 
**THIS IS NOW DEPRECATED**

See:
- tekton-tasks
- tekton-pipelines
- tekton-workspaces
- tekton-triggers


## Configuration
The configuration of the chart can be found in `values.yaml`. 

*NOTE* The global tag is required

### Applications

| Field                 | Description                                                                                                                               | Example Value                    | Required |
| :-------------------- | :---------------------------------------------------------------------------------------------------------------------------------------- | :------------------------------- | :------- |
| `name`                | The name of the application to create a trigger for                                                                                       | `my-app`                         | yes      |
| `pipeline`            | The name of the pipeline to use when building the application                                                                             | `build-java-app`                 | yes      |
| `sourceRepositoryUrl` | The repository containing the Source Code                                                                                                 | ``                               | yes      |
| `sourceRepositoryRef` | The branch, tag, hash, to pull when building the application                                                                              | `develop`                        | no       |
| `dockerfileLocation`  | The location of the Dockerfile to build the application. Defaults to `./Dockerfile`                                                       | `src/main/docker/Dockerfile.jvm` | no       |
| `updateHelmChart`     | Whether to update the Helm Chart (repository) after building or not. Defaults to `true`. All fields below are required when set to `true` | `true`                           | no       |
| `infraRepositoryUrl`  | The repository containing the Infrastructure as Code (IaC) (required when `updateHelmChart` is set to `true`)                             | ``                               | no/yes   |
| `infraRepositoryRef`  | The branch, tag, sha to use when pulling the IaC repository                                                                               | `development`                    | no/yes   |
| `imageField`          | The field in the Helm Chart `values.yaml` of the IaC repo containing the Image location                                                   | `.image.name`                    | no/yes   |
| `shaField`            | The field in the IaC Repository containing the SHA of the image                                                                           | `image.sha`                      | no/yes   |
